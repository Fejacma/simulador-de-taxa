package br.com.itau;

public class Investimento {
    private double capital;
    private int quantidadeMeses;

    public Investimento (double capital, int quantidadeMeses) {
        this.capital = capital;
        this.quantidadeMeses = quantidadeMeses;
    }

    public Investimento(){}

    public double getCapital() {
        return capital;
    }

    public void setCapital (double capital) {
        this.capital = capital;
    }

    public int getQuantidadeMeses() {
        return quantidadeMeses;
    }

    public void setQuantidadeMeses (int quantidadeMeses) {
        this.quantidadeMeses = quantidadeMeses;
    }
}
