package br.com.itau;

public class Simulador {
    private Investimento investimento;
    private double taxaJuros;

    public Simulador (Investimento investimento, double taxaJuros){
        this.investimento = investimento;
        this.taxaJuros = taxaJuros;
        }

    public double taxaJuros () {
        return (investimento.getCapital() * Math.pow(((taxaJuros / 100) + 1), investimento.getQuantidadeMeses()));
    }
}





