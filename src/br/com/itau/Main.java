package br.com.itau;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.println("Informe o valor a ser investido: ");
        double capital = input.nextDouble();

        System.out.println("Informe a quantidade de meses desejado: ");
        int quantidadeMeses = input.nextInt();

        Investimento investimento = new Investimento(capital, quantidadeMeses);

        Simulador simulador = new Simulador(investimento, 0.7);

        System.out.println("Valor final: " + simulador.taxaJuros());
    }
}




